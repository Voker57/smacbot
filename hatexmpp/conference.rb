require_relative '../hatexmpp'

class Hatexmpp
class Conference
	def initialize(hatexmpp, jid)
		@jid = jid
		@hatexmpp = hatexmpp
	end
	
	def listen
		buffer = ""
		f = File.open("#{@hatexmpp.dir}/roster/#{@jid}/__chat", "r")
		f.seek(0, :END)
		loop do
			if !f.eof?
				r =  f.read_nonblock(1000)
				r.force_encoding(Encoding::UTF_8)
				data = buffer.to_s + r
				a = data.split("\n")
				if !data.end_with? "\n"
					buffer = a.pop
				end
				a.each do |d|
					m = Hatexmpp::Message.parse(d)
					yield m if m
				end
			else
				pos = f.pos
				f = File.open("#{@hatexmpp.dir}/roster/#{@jid}/__chat", "r")
				f.seek pos
				sleep 1
			end
			
		end
	end
	
	def users
		Dir.entries("#{@hatexmpp.dir}/roster/#{@jid}").reject {|v| (v.start_with? "__") || (v.start_with? ".")}
	end
	
	def say(message)
		File.open("#{@hatexmpp.dir}/roster/#{@jid}/__chat", "a") do |f|
			f.syswrite message
		end
	end
end
end
