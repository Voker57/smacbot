
class Hatexmpp
class Message
	def self.parse(str)
		data = str.scan(/\d+ jid .*? nick (.*?) body \{(.*?)\}/)
		if data.empty?
			return nil
		else
			Message.new(data[0][0].gsub('\n', "\n"), data[0][1].gsub('\n', "\n"))
		end
	end
	
	def initialize(nick, body)
		@nick = nick
		@body = body
	end
	
	def nick
		@nick
	end
	
	def body
		@body
	end
end
end
