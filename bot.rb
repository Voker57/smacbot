require 'open3'
require 'yaml'
require 'fileutils'
require_relative './hatexmpp'
require 'shellwords'


if !ARGV[0]
	STDERR.puts "argument must be bot runtime dir"
	exit 1
end

DIR = ARGV[0]

Thread.abort_on_exception = true

config = YAML.load(File.read("#{DIR}/config.yml"))
$council_state = nil

def handle_command(hx, msg, source, cmd, args)
	council = lambda do if $council_state
			yea = $council_state[:votes].select{|k,v| v == "YEA"}.keys
			nay = $council_state[:votes].select{|k,v| v == "NAY"}.keys
			abstain = $council_state[:votes].select{|k,v| v == "ABSTAIN"}.keys
			result = if yea.count > nay.count
				"YEA"
			else 
				"NAY"
			end
			
			"Current vote for #{$council_state[:motion]}: #{result}\n\n" +
			"YEA: #{yea.count} #{yea.join(", ")}\n" +
			"NAY: #{nay.count} #{nay.join(", ")}\n" +
			"ABSTAIN: #{abstain.count} #{abstain.join(", ")}"
		else
			""
		end
	end
	
	case cmd
	when "r"
		if args.empty?
			source.say("READY?! #{source.users.join(", ")}")
		else
			source.say("READY for #{args[0]}?! #{source.users.join(", ")}")
		end
	when "smac"
		hoster = rand((args[0] || 2).to_i)

		alllist = "Spartan Federation
Gaia's Stepdaughters
University of Planet
Peacekeeping Forces
Human Hive
Lord's Believers
Morgan Industries
Cult of Planet
Cybernetic Consciousness
Data Angels
Free Drones
Nautilus Pirates
Manifold Usurpers
Manifold Caretakers".split("\n")

		faclist = alllist.shuffle[0..6]

		while (faclist.include? "Manifold Usurpers") ^ (faclist.include? "Manifold Caretakers")
			faclist = alllist.shuffle[0..6]
		end

		source.say "Player #{hoster} is hosting\n\n#{ faclist.join("\n")}"
	when "civ5"
		alllist = ["George Washington", "Harun Ar-Rashid", "Ashurbanipal", "Maria Theresa", "Nebuchadnezzar", "Pedro II", "Theodora", "Dido", "Wu Zetyan", "Harald Bluetooth", "Ramesses II", "Elizabeth", "Hailie Selassie", "Napoleon", "Bismarck", "Alexander the Great", "Gandhi", "Gajah Mada", "Oda Nobunaga", "Sejong the Great", "Genghis Khan", "Ahmad al-Mansur", "Darius I", "Casimir III", "Kamehameha", "Maria I", "Augustus Caesar", "Catherine", "Ramkhamhaeng", "Askia", "Isabella", "Gustavus Adolphus", "Montezuma", "Boudicca", "Attila", "Pachacuti", "Hiawatha", "Pacal", "William", "Suleiman", "Pocatello", "Shaka", "Erico Dandolo"]
		
		banned = ["Isabella", "Hailie Selassie", "Erico Dandolo"] 

		faclist = (alllist-banned).shuffle.zip(args)[0..args.count-1]
		
		source.say "Banned nations: #{banned.join(", ")}\n\n#{faclist.map{|f, pl| "#{pl}: #{f}"}.join("\n")}"
	when "rand"
		source.say args.sample
	when "vote"
		if args.empty?
			source.say("Vote on what?")
		else
			$council_state = {motion: args.join(" "), nick: msg.nick, votes: {}}
			source.say("PLANETARY COUNCIL PROPOSAL: #{args.join}
			#{source.users.join(", ")}, vote YEA/NAY/ABSTAIN")
		end
	when "endvote"
		if $council_state
			source.say("Vote ended!\n"+council.call)
			$council_state = nil
		else
			source.say("There's no vote, dummy")
		end
	when "YEA", "NAY", "ABSTAIN"
		if $council_state
			$council_state[:votes] ||= {}
			$council_state[:votes][msg.nick] = cmd
			source.say(council.call)
		end
	end
end

def process_chatline(c, str)
	nstr = str.dup
	c.users.each do |uname|
		nstr = nstr.gsub(/^#{Regexp.escape(uname)}[:,]\s+/, "")
	end
	return nstr
end

def markov_learn(str)
	IO.popen("hmark '#{DIR}/bot' train", "r+") do |i|
		i.puts(str)
		i.close_write
		i.read
	end
end

def markov_react(str)
	rstr = nil
	IO.popen("hmark '#{DIR}/bot' burst", "r+") do |i|
		i.puts(str)
		i.close_write
		rstr = i.read
	end
	if rstr.empty?
		rstr = nil
	end
	rstr.strip
end

FileUtils.mkdir_p DIR
h = Hatexmpp.new("#{DIR}/fs", config)
lq = Thread.new(h.log_queue) do |queue|
	puts "[HATE] #{queue.pop}"
end
h.connect!
c = h.join_conference("smac@conference.bitcheese.net")



c.listen do |m|
	next if m.nick == h.config[:muc_default_nick]
	if !(a = m.body.scan(/^#{Regexp.escape config[:symbol]}(\w+)(.*?)$/)).empty?
		handle_command(h, m, c, a[0][0], Shellwords.split(a[0][1]))
	else
		if rand < config[:chattiness] or m.body.start_with? h.config[:muc_default_nick]
			str = process_chatline(c, m.body)
			rstr = markov_react(str)
			if rstr
				c.say(rstr)
			end
		end
		markov_learn(process_chatline(c, m.body))
	end
end

lq.join
