require_relative './hatexmpp/message'
require_relative './hatexmpp/conference'

class Hatexmpp
	def initialize(dir, config)
		@dir = dir
		FileUtils.mkdir_p @dir
		@config = config
		@conferences = {}
		@log_queue = Queue.new
		@hate_inner_thread = nil
		@hate_thread = Thread.new(@killpipe) do |killpipe|
			sin, sout, thr = Open3.popen2e("hatexmpp", dir, "-f")
			sout.each_line do |hateful_message|
				@log_queue << hateful_message
			end
			@hate_inner_thread = thr
			thr.join
		end
		ObjectSpace.define_finalizer(self, Proc.new{puts "oops"; @hate_inner_thread.exit})
		sleep 5 # Get your shit and move, Hate

		config.each do |k, v|
			File.open("#{@dir}/config/#{k}", "w") do |f|
				f.write v.to_s
			end
		end
		sleep 1 # Read the fucking config, Hate
	end
	
	def log_queue
		@log_queue
	end
	
	def config
		@config
	end
	
	def dir
		@dir
	end
	
	def conferences
		@conferences
	end
	
	def connect!
		FileUtils.mkdir_p "#{@dir}/roster"
		sleep 2 # Connect to fucking server, Hate
	end
	
	def join_conference(jid)
		FileUtils.mkdir_p "#{@dir}/roster/#{jid}"
		sleep 3 # Join the fucking room, Hate
		@conferences[jid] = Conference.new(self, jid)
	end
	
end
